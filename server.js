const express = require("express");
const fs = require('fs');
const app = express();
const PORT = 3000;

// Initialisointi tiedostosta lukemista varten
const readFileLines = sanakirja =>
   fs.readFileSync(sanakirja)
   .toString('UTF8')
   .split('\n');

// Luetaan tekstitiedoston rivit ja tehdään sanapareista map
let data = readFileLines('sanakirja.txt');
const sanat = new Map();

data.forEach(line => {
  const myArray = line.split(" ");
  sanat.set(myArray[0], myArray[1]);
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/*CORS isn’t enabled on the server, this is due to security reasons by default,
so no one else but the webserver itself can make requests to the server.*/
// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  res.setHeader("Content-type", "application/json");

  // Pass to next layer of middleware
  next();
});

// GET sana
app.get("/kaannos/word", (req, res) => {
  const word = req.query.word;
  // res.json(sanat.get(word));
  console.log(word + " on englanniksi " + sanat.get(word));
  res.json(sanat.get(word));
  });
  
// ADD sana
app.post("/lisaa", (req, res) => {
  const word = req.body;
  sanat.set(word.suomeksi, word.englanniksi);
  fs.appendFile('sanakirja.txt', '\n' + word.suomeksi + " " + word.englanniksi, function (err) {
    if (err) {
      console.log("Error: Lisääminen ei onnistunut");
    } else {
      console.log("Lisättiin " + word.suomeksi + " " + word.englanniksi);
    }
  })
});

// Yhteys serveriin
app.listen(PORT, () => console.log(`Server running on port: http://localhost${PORT}`));

